package main

import (
	"fmt"
	"html/template"
	"os"
	"strings"

	"github.com/tealeg/xlsx"
)

// Winner structure
type Winner struct {
	Number int
	City   string
	Tubing bool
}

// Returns parameters from the row
func parse(cells []*xlsx.Cell) (int, string, bool) {
	number, _ := cells[0].Int()

	var tubing bool

	if len(cells) < 3 {
		tubing = false
	} else {
		tubing = strings.Trim(strings.ToLower(cells[2].String()), " ") == "полотенце"
	}

	return number, cells[1].String(), tubing
}

// Main function
func main() {
	var winners []Winner

	fileName := "winners.xlsx"

	file, err := xlsx.OpenFile(fileName)

	if err != nil {
		fmt.Println("An error occurred!")
	}

	for _, sheet := range file.Sheets {
		for _, row := range sheet.Rows {
			cells := row.Cells
			number, city, tubing := parse(cells)
			winners = append(
				winners,
				Winner{
					Number: number,
					City:   strings.Trim(city, " "),
					Tubing: tubing,
				},
			)
		}
	}

	// Parse the template
	t, err := template.ParseFiles("template.html")
	if err != nil {
		fmt.Println("(Template error)", err)
		return
	}

	// Create the file
	f, err := os.Create("winners.html")
	if err != nil {
		fmt.Println("(Create error)", err)
		return
	}

	// Execute the template
	err = t.Execute(f, winners)
	if err != nil {
		fmt.Print("(Execute error)", err)
		return
	}
}
